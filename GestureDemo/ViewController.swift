//
//  ViewController.swift
//  GestureDemo
//
//  Created by Test User on 2019. 10. 20..
//  Copyright © 2019. NJE GAMF. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var mainImageView: UIImageView!  {
        didSet {
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.tap(recognizer:)))
            mainImageView.addGestureRecognizer(tapGestureRecognizer)
            
            let swipeRightGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(ViewController.swipe(recognizer:)))
            swipeRightGestureRecognizer.direction = UISwipeGestureRecognizer.Direction.right
            mainImageView.addGestureRecognizer(swipeRightGestureRecognizer)
            
            let swipeLeftGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(ViewController.swipe(recognizer:)))
            swipeLeftGestureRecognizer.direction = UISwipeGestureRecognizer.Direction.left
            mainImageView.addGestureRecognizer(swipeLeftGestureRecognizer)
            
            let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(ViewController.longPress(recognizer:)))
            longPressGestureRecognizer.minimumPressDuration = 1
            longPressGestureRecognizer.delaysTouchesBegan = true
            mainImageView.addGestureRecognizer(longPressGestureRecognizer)
        }
    }
    
    @objc func tap(recognizer: UITapGestureRecognizer) {
        switch recognizer.state {
        case .ended:
            print("tap")
            next()
        default:
            break
        }
    }
    
    @objc func swipe(recognizer: UISwipeGestureRecognizer) {
        switch recognizer.direction {
        case .left:
            print("swipe left")
            next()
        case .right:
            print("swipe right")
            previous()
        default:
            break
        }
    }
    
    @objc func longPress(recognizer: UILongPressGestureRecognizer) {
        switch recognizer.state {
        case .ended:
            print("long press")
            takePhoto()
        default:
            break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // user interaction is not enabled in image view by default
        mainImageView.isUserInteractionEnabled = true
    }
    
    var imagePicker: UIImagePickerController!
    
    func takePhoto() {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
        } else {
            print("Camera is not available")
            imagePicker.sourceType = .photoLibrary
        }
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        mainImageView.image = info[.originalImage] as? UIImage
    }
    
    var heroes: [String] = ["01", "02", "03"]
    var current: Int = 0

    func next() {
        current+=1
        if current >= heroes.count {
            current = 0
        }
        mainImageView.image = UIImage(named: heroes[current])
    }
    
    func previous() {
        current-=1
        if current < 0 {
            current = heroes.count - 1
        }
        mainImageView.image = UIImage(named: heroes[current])
    }
}

